package git_taschenrechner;

public class Taschenrechner {

	public double add(double zahl1, double zahl2) {
		double ergebnis = zahl1 + zahl2;
		return ergebnis;
	}
	
	public double sub(double zahl1, double zahl2){
		double ergebnis = zahl1 - zahl2;
		return ergebnis; 
	}
	
	public double mul(double zahl1, double zahl2){
		double ergebnis = zahl1 * zahl2;
		return ergebnis;
	}

	public double div(double zahl1, double zahl2){
		double ergebnis = zahl1 / zahl2;
		return ergebnis;
	}
	
}
