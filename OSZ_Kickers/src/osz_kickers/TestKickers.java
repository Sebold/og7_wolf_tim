package osz_kickers;

public class TestKickers {

	public static void main(String[] args) {
	Spieler Testspieler = new Spieler();
	Schiedsrichter Testschiedsrichter = new Schiedsrichter();
	Mannschaftsleiter Testmannschaftsleiter = new Mannschaftsleiter();
	Trainer Testtrainer = new Trainer();
	
	Testspieler.setName("Bernd");
	Testspieler.setSpielposition("Mittelfeldspieler");
	Testspieler.setTrikotnummer(9);
	Testspieler.setTelefonnummer(1234567890);
	Testspieler.setBeitraggezahlt(false);
	
	Testschiedsrichter.setName("Mike");
	Testschiedsrichter.setTelefonnummer(1324567980);
	Testschiedsrichter.setGepfiffeneSpiele(3);
	Testschiedsrichter.setBeitraggezahlt(true);
	
	Testmannschaftsleiter.setName("J�rg");
	Testmannschaftsleiter.setMannschaftsname("SV TestVerein");
	Testmannschaftsleiter.setRabatt(35);
	Testmannschaftsleiter.setSpielposition("St�rmer");
	Testmannschaftsleiter.setTelefonnummer(1234567809);
	Testmannschaftsleiter.setTrikotnummer(6);
	Testmannschaftsleiter.setBeitraggezahlt(true);
	
	Testtrainer.setName("Wolfgang");
	Testtrainer.setTelefonnummer(1452367890);
	Testtrainer.setLizenzklasse('A');
	Testtrainer.setAufwandentschaedigung(450);
	Testtrainer.setBeitraggezahlt(true);
	
	
	
	System.out.println(Testspieler.getName());
	System.out.println(Testspieler.getSpielposition());
	System.out.println(Testspieler.getTrikotnummer());
	System.out.println(Testspieler.getTelefonnummer());
	System.out.println(Testspieler.isBeitraggezahlt());
	
	System.out.println("");
	
	System.out.println(Testschiedsrichter.getName());
	System.out.println(Testschiedsrichter.getTelefonnummer());
	System.out.println(Testschiedsrichter.getGepfiffeneSpiele());
	System.out.println(Testschiedsrichter.isBeitraggezahlt());
	
	System.out.println("");
	
	System.out.println(Testmannschaftsleiter.getName());
	System.out.println(Testmannschaftsleiter.getSpielposition());
	System.out.println(Testmannschaftsleiter.getTelefonnummer());
	System.out.println(Testmannschaftsleiter.getMannschaftsname());
	System.out.println(Testmannschaftsleiter.getRabatt()+ " %");
	System.out.println(Testmannschaftsleiter.getTrikotnummer());
	System.out.println(Testmannschaftsleiter.isBeitraggezahlt());
	
	System.out.println("");
	
	System.out.println(Testtrainer.getName());
	System.out.println(Testtrainer.getTelefonnummer());
	System.out.println(Testtrainer.getLizenzklasse());
	System.out.println(Testtrainer.getAufwandentschaedigung()+ " Euro");
	System.out.println(Testtrainer.isBeitraggezahlt());
	}

}
