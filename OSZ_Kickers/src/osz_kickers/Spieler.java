package osz_kickers;

public class Spieler extends Person{

	
	protected int trikotnummer;
	protected String spielposition;
	
	
	public Spieler() {}


	public int getTrikotnummer() {
		return trikotnummer;
	}


	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}


	public String getSpielposition() {
		return spielposition;
	}


	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}
	
	
	
}
