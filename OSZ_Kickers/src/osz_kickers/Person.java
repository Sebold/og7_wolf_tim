package osz_kickers;

public abstract class Person {
	
	
protected String name;
protected int telefonnummer;
protected  boolean beitraggezahlt;


public  Person() {
	
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public int getTelefonnummer() {
	return telefonnummer;
}


public void setTelefonnummer(int telefonnummer) {
	this.telefonnummer = telefonnummer;
}


public boolean isBeitraggezahlt() {
	return beitraggezahlt;
}


public void setBeitraggezahlt(boolean beitraggezahlt) {
	this.beitraggezahlt = beitraggezahlt;
}


}