package osz_kickers;

public class Mannschaftsleiter extends Spieler {

	
	private String mannschaftsname;
	private int rabatt;
	
	
	public Mannschaftsleiter() {}


	public String getMannschaftsname() {
		return mannschaftsname;
	}


	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}


	public int getRabatt() {
		return rabatt;
	}


	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	
	
}
