package arrays;

import java.util.Arrays;

/**
 *
 * uebungsklasse zu Feldern
 *
 * @version 1.0 vom 05.05.2011
 * @author Tenbusch
 */

public class Felder {

	// unsere Zahlenliste zum Ausprobieren
	private int[] zahlenliste = { 5, 8, 4, 3, 9, 1, 2, 7, 6, 0 };

	// Konstruktor
	public Felder() {
	}

	// Methode die Sie implementieren sollen
	// ersetzen Sie den Befehl return 0 durch return ihre_Variable

	// die Methode soll die groeste Zahl der Liste zurueckgeben
	public int maxElement() {
		int x = zahlenliste.length;
		int y = zahlenliste[1];
		for (int i = 0; i < x; i++) {
			if (zahlenliste[i] > y) {
				y = zahlenliste[i];
			}
		}

		return y;
	}

	// die Methode soll die kleinste Zahl der Liste zurueckgeben
	public int minElement() {
		int x = zahlenliste.length;
		int y = zahlenliste[1];
		for (int i = 0; i < x; i++) {
			if (zahlenliste[i] < y) {
				y = zahlenliste[i];
			}
		}
		return y;
	}

	// die Methode soll den abgerundeten Durchschnitt aller Zahlen zurueckgeben
	public int durchschnitt() {
		int x = zahlenliste.length;
		int y = 0;
		for (int i = 0; i < x; i++) {
			y = y + zahlenliste[i];
		}
		y = y / x;
		return y;
	}

	// die Methode soll die Anzahl der Elemente zurueckgeben
	// der Befehl zahlenliste.length; koennte hierbei hilfreich sein
	public int anzahlElemente() {
		int x = zahlenliste.length;
		return x;
	}

	// die Methode soll die Liste ausgeben
	public String toString() {
		String x = "";
		x = Arrays.toString(zahlenliste);
		return x;
	}

	// die Methode soll einen booleschen Wert zurueckgeben, ob der Parameter in
	// dem Feld vorhanden ist
	public boolean istElement(int zahl) {

		int x = zahlenliste.length;
		int y = zahl;
		for (int i = 0; i < x; i++) {
			if (zahlenliste[i] == y) {
				return true;

			}

		}
		return false;
	}

	// die Methode soll das erste Vorkommen der
	// als Parameter uebergebenen Zahl liefern oder -1 bei nicht vorhanden
	public int getErstePosition(int zahl) {
        int x = zahlenliste.length;
        int y = zahl;
        for(int i = 0; i<x; i++) {
        if(zahlenliste[i] == y) {
        y = i;
        i = i+x;
        }
        }
		return y;
	}

	// die Methode soll die Liste aufsteigend sortieren
	// googlen sie mal nach Array.sort() ;)
	public void sortiere() {
    Arrays.sort(zahlenliste);
    
		
		
	}

	public static void main(String[] args) {
		Felder testenMeinerLösung = new Felder();
		System.out.println(testenMeinerLösung.maxElement());
		System.out.println(testenMeinerLösung.minElement());
		System.out.println(testenMeinerLösung.durchschnitt());
		System.out.println(testenMeinerLösung.anzahlElemente());
		System.out.println(testenMeinerLösung.toString());
		System.out.println(testenMeinerLösung.istElement(9));
		System.out.println(testenMeinerLösung.getErstePosition(5));
		testenMeinerLösung.sortiere();
		System.out.println(testenMeinerLösung.toString());
	}
}