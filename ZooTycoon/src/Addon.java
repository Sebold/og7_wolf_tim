
public class Addon {
private double verkaufspreis;
private int eindeutigeID;
private int bestand;
private String bezeichnung;
private int maxBestand;

//-----------------------------------------------------------//


public double getVerkaufspreis() {	
	return verkaufspreis;
}
public void setVerkaufspreis(double verkaufspreis) {
	this.verkaufspreis = verkaufspreis;
}
public int getEindeutigeID() {
	return eindeutigeID;
}
public void setEindeutigeID(int eindeutigeID) {
	this.eindeutigeID = eindeutigeID;
}
public int getBestand() {
	return bestand;
}
public void setBestand(int bestand) {
	this.bestand = bestand;
}
public String getBezeichnung() {
	return bezeichnung;
}
public void setBezeichnung(String bezeichnung) {
	this.bezeichnung = bezeichnung;
}
public int getMaxBestand() {
	return maxBestand;
}
public void setMaxBestand(int maxBestand) {
	this.maxBestand = maxBestand;
}
}
